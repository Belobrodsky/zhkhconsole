﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ZHKH
{
    class MySmtpSend
    {
        /// <summary>
        /// Статический метод для отправки почты
        /// </summary>
        /// <param name="smtpServer">Сервер для отправки почты</param>
        /// <param name="from">Адрес отправителя</param>
        /// <param name="password">Пароль</param>
        /// <param name="mailto">Адрес получателя</param>
        /// <param name="caption">Тема сообщения</param>
        /// <param name="message">Само сообщение</param>
        /// <param name="attachFile"></param>
        public static void SendMail(string smtpServer, string from, string password,
        string mailto, string caption, string message, string attachFile = null)
        {
                  MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                mail.To.Add(new MailAddress(mailto));
                mail.Subject = caption;
                mail.Body = message;
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                client.Port = 25;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(from, password);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                mail.Dispose();


        }
    }
}
