﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHKH
{
    class Const
    {

        private static List<string> _russianMouns = new List<string>() { "январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь","ноябрь", "декабрь" };

        public static string CurrentMouns
        {
            get
            {
                int m;
                if (DateTime.Now.Month == 0) //Платим же мы за предыдущий месяц. для 1 месяца предыдущий является 12
                {
                    m = 11; //Декабрь как ни странно 11 месяц так как индексация идет с 0
                }
                else m = DateTime.Now.Month-2;              
                return _russianMouns[m];
            }

        }

    }
}
