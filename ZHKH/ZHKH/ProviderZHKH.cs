﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHKH
{
    
   /// <summary>
   /// Класс для описания абстрактного провайдера услуг ЖКХ
   /// </summary>
    class ProviderZHKH
    {

        private string _desc;

        public string Desc
        {
            get { return _desc; }
            set { _desc = value; }
        }

        private string _ls;
        public string Ls
        {
        get {return _ls;}
        set {_ls=value;}
        }
              
        
        
        string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }


        string _msgSubject;

        public string MsgSubject
        {
            get { return _msgSubject; }
            set { _msgSubject = value; }
        }
        string _msgBodyMask;

        public string MsgBodyMask
        {
            get { return _msgBodyMask; }
            set { _msgBodyMask = value; }
        }

    }
}
